app.controller('roomCtrl', ['$scope','$routeParams','roomService',
    function ($scope,$routeParams ,roomService) {

       var searchKey = Number($routeParams.roomRouteKey);
       //could be done as $scope.
        $scope.room;
        $scope.rooms;
        $scope.roomAddingStatus = false;
        $scope.roomChanged = false;
        $scope.AroomName = "defaultroom";
        $scope.AroomSize = "defaultroomsize";
        $scope.Akey = 00111;



        $scope.findRoom = function(roomKey) {
            roomService.showRoom(roomKey)
            .then(function (response) {
                $scope.room = response.data;
                $scope.roomName = $scope.room.roomName;
            }, function (error) {
                $scope.status = 'Unable to load room data: ' + error.message;
            });
          }

        $scope.addRoom = function(){
            $scope.roomAddingStatus = true;
            newRoom = {
                roomName : $scope.AroomName,
                roomSize : $scope.AroomSize,
                key : $scope.Akey
            }
            roomService.showRoom($scope.Akey)
            .then(function (response) {
                $scope.checkRoom = response.data;
                if ($scope.checkRoom.key != $scope.Akey) {
                    $scope.myVar = true;
            roomService.addRoom(newRoom)
            .then(function(response) {
             $scope.roomInsertionstatus = 'added room successfully!';
             $scope.rooms.push(newRoom);
             }, function(error) {
                 $scope.roomInsertionstatus = 'unable to add room! :' + error.message;
             }); 
            }
              else {
                $scope.roomInsertionstatus = 'Room with this id is already exist!';
            } 
        }, 
        
        function (error) {
            $scope.roomInsertionstatus = 'Unable to load room data: ' + error.message;
        }
            ) }

 $scope.showRoomInfo = function () {
    roomService.showRoom(searchKey)
    .then(function (response) {
        $scope.room = response.data;
        $scope.roomName = $scope.room.roomName;
        $scope.roomSize = $scope.room.roomSize;
        $scope.key = $scope.room.key;
    },
    
    function (error) {
        $scope.status = 'Unable to load room data: ' + error.message;
    }
    
    );
    
   
    }
    $scope.updateRoom = function(roomName, roomSize, key) {
        $scope.roomChanged = true;
        roomModified = {
                 roomName : roomName,
                 roomSize :  roomSize,
                 key : key
         }
        roomService.updateRoom(roomModified)
            .then(function (response) {
                $scope.updateStatus = 'successfully updated room, please refresh the page!';
            }, function (error) {
                $scope.updateStatus = 'Unable to update room : ' + error.message;
            });

    }

}]);