app.controller('reservationCtrl', ['$scope', '$routeParams', 'reservationService','personService','$mdDialog', 
    function ($scope, $routeParams, reservationService ,personService,$mdDialog,) {

        $scope.startDate ='';
        $scope.endDate ='';
        $scope.isOpen = false;
       
        $scope.persons = [];
        $scope.ReservationInsertionStatus
        $scope.roomKey = Number($routeParams.roomKey);

        var flag = 1;
        $scope.personKey;
        $scope.firstName;
        $scope.selectedName = "faheem";
        $scope.reservations = [];
        $scope.reservationAdding = false;

        $scope.min = 1;
        $scope.max = 100000000;
        if ($scope.min > $scope.max) {
            [$scope.min, $scope.max] = [$scope.max, $scope.min];
        }

      

        $scope.showPersons = function(){
            personService.showPersons()
            .then(function (response) {
                $scope.persons = response.data;
            }, function (error) {
                $scope.status = 'Unable to load person data: ' + error.message;
            });
        }

      

            $scope.addReservation = function ($event) {
                $scope.reservationAdding = true;
    
                newReservation = {
                    key: Math.floor(Math.random() * ($scope.max - $scope.min + 1) + $scope.min),
                    personKey: $scope.selectedName,
                    roomKey: $scope.roomKey,   
                    startTime: $scope.startDate,
                    endTime: $scope.endDate,
                     
                }    
                     
                               if(newReservation.startTime == newReservation.endTime)
                                {
                                    $window.alert('please choose the time period to make reservations!')
                                }
                                else {
                            reservationService.addReservation(newReservation)
                                .then(function (response) {
                                    $scope.reservations.push(newReservation);
                                    $scope.ReservationInsertionStatus = 'reservation inserted successfully!';                                  
                                }, function (error) {
                                    $scope.ReservationInsertionStatus = 'cannot make reservation at the specified times! ';
                                });
                            }
    
                    
            }
    
            $scope.deleteAllReservations = function(ev) {
              
                var confirm = $mdDialog.confirm()
                      .title('Are you sure?')
                      .textContent('You are about to remove all the reservations.')
                      .ariaLabel('Lucky day')
                      .targetEvent(ev)
                      .ok('Yes')
                      .cancel('No');
            
                $mdDialog.show(confirm).then(function() {
                  $scope.status = 'You decided to remove all reservations.';
                  reservationService.showReservations()
                .then(function (response) {
                    $scope.reservations = response.data;
                    if($scope.reservations.length!=0) {
                     
                        reservationService.deleteAllReservations()
                        .then(function (response) {
                            $scope.reservations = response.data;
                            $scope.deleteAllStatus = 'Removed All reservations ';
        
                        }, function (error) {
                            $scope.deleteAllStatus = 'Unable to delete reservations: ' + error.message;
                        }
                        )
                    } else {
                        $scope.deleteAllStatus = 'There is no reservation in the system! ';
                    }

                }, function (error) {
                    $scope.status = 'Unable to load reservations: ' + error.message;
                }
                )


                










                }, function() {
                  $scope.status = 'You decided to keep all reservations.';
                });
              };
            
     
            
              
                
               
            
           

           
           
        $scope.showReservations = function () {
            $scope.showAllReservations = true;
            reservationService.showReservations()
                .then(function (response) {
                    $scope.reservations = response.data;

                }, function (error) {
                    $scope.status = 'Unable to load reservations: ' + error.message;
                }
                )
        };
        $scope.deleteReservation = function(ev,key) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                  .title('Are you sure?')
                  .textContent('you are about to delete the reservation.')
                  .ariaLabel('Lucky day')
                  .targetEvent(ev)
                  .ok('Yes')
                  .cancel('No');
        
            $mdDialog.show(confirm).then(function() {
              $scope.status = 'You decided to delete the reservation.';
                reservationService.deleteReservation(key).then(function (response) {
                    $scope.status = 'reservation removed from the list';
                    for (var i = 0; i < $scope.reservations.length; i++) {

                        if ($scope.reservations[i].key === key) {
                            $scope.reservations.splice(i, 1);
                            break;
                        }
                    }
                }, function (error) {
                    $scope.status = 'unable to remove reservation!' + error.message;
                });
       
            


            }, function() {
              $scope.status = 'You decided to keep it.';
            });
          };
       

     
      
    }]);