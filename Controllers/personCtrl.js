app.controller('personCtrl', ['$scope', '$routeParams', 'personService',
    function ($scope, $routeParams, personService) {

        $scope.searchKey = Number($routeParams.personRouteKey);
        $scope.person;
        $scope.persons;
        $scope.myVar = false;
        $scope.personAddingStatus = false;
        $scope.personChanged = false;
        //$scope.afterUpdatedMessage = "the person is changed!";

        $scope.AfirstName = "abc";
        $scope.AlastName = "xyz";
        $scope.Akey = 1235;


        $scope.findPerson = function (personKey) {
            personService.showPerson(personKey)
                .then(function (response) {
                    $scope.person = response.data;
                    $scope.firstName = $scope.person.firstName;
                }, function (error) {
                    $scope.status = 'Unable to load person data: ' + error.message;
                });
        }


        $scope.addPerson = function () {
            $scope.personAddingStatus = true;
            newPerson = {
                firstName: $scope.AfirstName,
                lastName: $scope.AlastName,
                key: $scope.Akey
            }

            personService.showPerson($scope.Akey)
                .then(function (response) {
                    $scope.checkPerson = response.data;
                    if ($scope.checkPerson.key != $scope.Akey) {
                        $scope.myVar = true;
                        personService.addPerson(newPerson)
                            .then(function (response) {
                                $scope.personInsertionstatus = 'added person successfully!';
                                $scope.persons.push(newPerson);
                            }, function (error) {
                                $scope.personInsertionstatus = 'unable to add person! :' + error.message;
                            });
                    }
                    else {
                        $scope.personInsertionstatus = 'Person with this id is already exist!';
                    }
                }, function (error) {
                    $scope.personInsertionstatus = 'Unable to load person data: ' + error.message;
                }

                )



        }



        $scope.showPersonInfo = function () {
            personService.showPerson($scope.searchKey)
                .then(function (response) {
                    $scope.person = response.data;
                    $scope.firstName = $scope.person.firstName;
                    $scope.lastName = $scope.person.lastName;
                    $scope.key = $scope.person.key;
                }, function (error) {
                    $scope.status = 'Unable to load person data: ' + error.message;
                });



        }
        $scope.updatePerson = function(firstName, lastName, key) {
            $scope.personChanged = true;
            personModified = {
                     firstName : firstName,
                     lastName :  lastName,
                     key : key
             }
            personService.updatePerson(personModified)
                .then(function (response) {
                    $scope.updateStatus = 'successfully updated person, please refresh the page!';
                }, function (error) {
                    $scope.updateStatus = 'Unable to update person : ' + error.message;
                });

        }

        

    }]);