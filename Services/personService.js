angular.module('app')
.factory('personService', ['$http', function ($http) {
 
    var personService = {};


    personService.showPersons = function (){
        return $http.get('http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/getAllPersonData');
    };


    personService.deletePerson = function (key) {
      var urlBase =  'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/deletePerson'; 
        return $http.delete(urlBase + '/' + key);

    };


    personService.addPerson = function (newPerson) {
        var urlBase = 'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/setData';
        return $http.post(urlBase, newPerson);

    };

    personService.showPerson = function (key){
        var urlBase = 'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/getData'; 
        return $http.get(urlBase + '/' + key);
    };

    personService.updatePerson = function(personModified){
        var urlBase = 'http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/updateData';
        return $http.put(urlBase, personModified);


    };
    
    return  personService;
}]);