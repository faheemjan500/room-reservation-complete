var routeApp = angular.module('routeApp', ["ngRoute"]) 
  
routeApp.config(['$routeProvider',

     function($routeProvider){
      $routeProvider
      .when("/", {
         templateUrl : "Practice2.html"
       })
      .when("/personInfoRoute",{
         templateUrl : "/person.html",
         controller: "displayPersonsCtrl"
      })
      .when("/roomInfoRoute", {
          templateUrl: "/room.html",
         controller: "displayRoomsCtrl"
     })
  }]);

  routeApp.controller("displayPersonsCtrl",function($scope){

     $scope.message = "hello persons";

 });
 routeApp.controller("displayRoomsCtrl",function($scope){

    $scope.message = "hello rooms";
 });
   